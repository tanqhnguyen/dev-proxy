SHELL := /bin/sh
.DEFAULT_GOAL:=help
.PHONY: help run

network: ## Create the dev-proxy network if not present
	@docker network create -d bridge dev-proxy || true

run: ## Run the proxy
	@docker-compose up -d

help: ## Display this help
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target>\033[0m\n\nTargets:\n"} /^[a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-10s\033[0m %s\n", $$1, $$2 }' $(MAKEFILE_LIST)
