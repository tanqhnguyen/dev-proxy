# What is this?

A dev proxy to run multiple services locally

# How to use it?

1. Create a new network via `make network`
2. Run the proxy using `make run`

For each new service, follow these steps to add it to the proxy

1. Define the following labels

```
labels:
  - traefik.enable=true
  - traefik.http.routers.my_route.rule=Host(`somehost.local`)
  - traefik.http.routers.my_route.entrypoints=web
  - traefik.http.routers.my_route.service=my_service
  - traefik.http.services.my_service.loadbalancer.server.port=80
```

2. Make sure the service belongs to `dev-proxy` network
3. Add `127.0.0.1 somehost.local` to `/etc/hosts` file
4. Profits

# Want HTTPS?

For HTTPS configuration, you will need to first generate a valid certificate. One easy way to do it is via [mkcert](https://github.com/FiloSottile/mkcert). Assuming that you have installed mkcert and generated a valid certificate, create a new file (for example `certs.yml`)  inside `configuration` to declare the certificates

```yml
tls:
  certificates:
    - certFile: /configuration/domain.com.pem
      keyFile: /configuration/domain.com-key.pem
```

Then in the service's docker-compose file, just enable tls in the labels

```
traefik.http.routers.my_route.tls=true
```

This is a result of [my blog post](https://tannguyen.org/2020/12/use-traefik-as-a-local-dev-proxy/) (missing the SSL part, though)
